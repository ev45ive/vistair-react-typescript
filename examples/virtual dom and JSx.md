```ts
import React from 'react';
import ReactDOM from 'react-dom';

window.React = React
window.ReactDOM = ReactDOM

interface Person {
  id: number;
  name: string;
}

const name = 'Kate '
const people: Person[] = [{ id: 123, name: 'Kate' }, { id: 234, name: 'Bob' }, { id: 345, name: 'Alice' }]

// const UserCard1 = (person: Person) => React.createElement('li', {
//   id: 'test', title: 'test', key: person.id,
//   style: { color: 'red', borderBottom: '1px solid red' },
//   className: ' person animal fancy '
// },
//   React.createElement('span', {}, person.name + ' has a cat '),
//   React.createElement('input', { placeholder: 'Pet' }),
// )

debugger

const UserCard = (props: { person: Person }) => <li
  title={props.person.name}
  style={{ color: 'red', borderBottom: '1px solid red' }}
  className="person animal fancy ">
  <span>{props.person.name} has a cat</span>
  <input type="text" placeholder="Add Pet" />
</li>

// const list = React.createElement('ul', {}, people.map(person => UserCard(person)));

const list = <ul id="list" title={"list " + " fancy "}>
  {people.map(listItem => <UserCard person={listItem} key={listItem.id} />)}

  {/* {UserCard({ person: people[0] })} */}
  {/* <UserCard person={people[0]} /> */}

  {/* {people.map(person => UserCard({ person: person }))} */}
</ul>


ReactDOM.render(list, document.getElementById('root'))
```