```ts
inc = (payload=1) => ({type:'INC', payload });
dec = (payload=1) => ({type:'DEC', payload });

addTodo = (payload) => ({type:'ADDTODO', payload });

function reduceCounter(state = 0, action){
    switch(action.type){
        case 'INC': return state + action.payload 
        case 'DEC': return  state - action.payload 
        default: return state;
    }
}

function reducer (state,action){
    switch(action.type){
//         case 'INC': return { ...state, counter: state.counter + action.payload };
//         case 'DEC': return { ...state, counter: state.counter - action.payload };
        case 'ADDTODO': return { ...state, todos:[...state.todos, action.payload] };
        default: return {
            ...state, 
            counter: reduceCounter(state.counter, action) 
        };
    }
}

state = {counter: 0, todos:[] };
state = reducer(state, inc())
// expect(state.counter).toEqual(...)

state = reducer(state, inc())
state = reducer(state, addTodo('buy milk!'))
state = reducer(state, dec())

// [inc(),inc(2),dec(),addTodo('buy milk!'), inc(2)].reduce(reducer, {
//     counter: 0,
//     todos:[]
// })

```
