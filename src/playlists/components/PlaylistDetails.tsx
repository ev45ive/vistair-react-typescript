import React from 'react'
import { Playlist } from '../../core/model/Playlist'

interface Props {
    playlist: Playlist
    onEdit(/* playlist_id: Playlist['id'] */): void
}

export const PlaylistDetails = ({ playlist, onEdit }: Props) => {

    return (
        <div>
            <dl title={playlist.name} data-playlist-id={playlist.id}>
                <dt>Name:</dt>
                <dd style={{
                    fontSize: 12 + (50 / playlist.name.length) + 'px'
                }}>{playlist.name}</dd>

                <dt>Public:</dt>
                <dd className={playlist.public ? 'text-danger' : 'text-success'}>
                    {playlist.public ? 'Yes' : 'No'}
                    {playlist.public && <span> Public !</span>}
                </dd>

                <dt>Description:</dt>
                <dd>{playlist.description.slice(0, 50)}</dd>
            </dl>
            <button className="btn btn-info" onClick={onEdit}>Edit</button>
        </div>
    )
}
