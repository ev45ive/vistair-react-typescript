import { useState, useEffect } from "react"
import { AlbumView } from "../model/Search"

const mockAlbums: AlbumView[] = [
    { id: '123', name: 'Album 123', images: [{ height: 300, width: 300, url: 'https://www.placecage.com/c/300/300' }] },
    { id: '234', name: 'Album 234', images: [{ height: 300, width: 300, url: 'https://www.placecage.com/c/400/400' }] },
    { id: '345', name: 'Album 345', images: [{ height: 300, width: 300, url: 'https://www.placecage.com/c/500/500' }] },
]

export const useFetch = <P, R>(fetcher: (params: P) => Promise<R>) => {

    const [results, setResults] = useState<R | undefined>()
    const [message, setMessage] = useState('')
    const [query, setQuery] = useState<P | undefined>()
    const [isLoading, setIsLoading] = useState(false)

    useEffect(() => {
        if (!query) return;

        setIsLoading(true)
        setMessage('')
        fetcher(query).then(result => setResults(result))
            .catch(error => setMessage(error.message))
            .finally(() => setIsLoading(false))
    }, [query])

    return [{ results, message, isLoading }, setQuery] as const // Tuple!
}