
interface AuthConfig {
    /**
     * Url for redirect
     * https://developer.spotify.com/documentation/general/guides/authorization-guide/#implicit-grant-flow
     * 
     */
    url: string
    client_id: string
    redirect_uri: string
    response_type: 'token'
    scopes: string[]
    show_dialog: boolean
}

export class AuthService {
    private token: string = ''

    constructor(private config: AuthConfig) { }

    init() {
        let token = new URLSearchParams(window.location.hash).get('#access_token')
        if (token) {
            sessionStorage.setItem('token', JSON.stringify(token))
            window.location.hash = ''
            this.token = token
        }

        if (!token) {
            const rawToken = sessionStorage.getItem('token')
            this.token = rawToken && JSON.parse(rawToken)
        }

        if (!this.token) {
            this.login()
        }
    }

    getToken() {
        return this.token
    }

    login() {
        const { client_id, redirect_uri, response_type, scopes, show_dialog } = this.config
        const params = new URLSearchParams({
            client_id, redirect_uri, response_type,
            scope: scopes.join(' '),
            show_dialg: show_dialog ? 'true' : 'false'
        })
        window.location.replace(`${this.config.url}?${params.toString()}`)
    }

    logout() {
        this.token = ''
        sessionStorage.removeItem('token')
    }
}
