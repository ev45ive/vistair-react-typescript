## GIT
cd ..
git clone https://bitbucket.org/ev45ive/vistair-react-typescript.git react-training
cd  react-training
npm i 
npm start

npm start -- --port 12345

git stash -u
git pull -u origin master


https://nodejs.org/en/
https://code.visualstudio.com/
https://github.com/facebook/create-react-app

node -v
v14.16.1

npm -v
6.14.6

git --version
git version 2.31.1.windows.1

code -v
1.56.0
cfa2e218100323074ac1948c885448fdf4de2a7f

## Vs Code plugins
https://marketplace.visualstudio.com/items?itemName=dsznajder.es7-react-js-snippets
https://marketplace.visualstudio.com/items?itemName=quicktype.quicktype


## Chrome plugins 
https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi


## Create react app

npm install --global create-react-app
npm i -g create-react-app
C:\Users\<USER>\AppData\Roaming\npm\create-react-app -> C:\Users\<USER>\AppData\Roaming\npm\node_modules\create-react-app\index.js 

create-react-app --help

npx create-react-app vistair-react-ts --template typescript

cd .. 
create-react-app vistair-react-ts --template typescript

cd vistair-react-ts 
npm start

Compiled successfully!

You can now view vistair-react-ts in the browser.

  http://localhost:3000


## Playlists

mkdir -p src/core/model
mkdir -p src/playlists/containers
mkdir -p src/playlists/components

touch src/playlists/containers/PlaylistsView.tsx

touch src/playlists/components/PlaylistList.tsx
touch src/playlists/components/PlaylistDetails.tsx
touch src/playlists/components/PlaylistForm.tsx

## Music Search
mkdir -p src/search/containers
mkdir -p src/search/components

touch src/search/containers/AlbumSearchView.tsx

touch src/search/components/AlbumCard.tsx
touch src/search/components/AlbumGrid.tsx
touch src/search/components/SearchForm.tsx



## Router
 npm i react-router-dom @types/react-router-dom


 