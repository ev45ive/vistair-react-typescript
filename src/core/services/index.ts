import axios from "axios";
import { AuthService } from "./AuthService";
import { SearchAlbumsService } from "./SearchService";

export const auth = new AuthService({
    url: 'https://accounts.spotify.com/authorize',
    client_id: '0577660799784b2684d4b65384a599d6',
    redirect_uri: 'http://localhost:3000/',
    response_type: 'token',
    scopes: [],
    show_dialog: true
})

auth.init()

axios.interceptors.request.use(request => {
    request.headers['Authorization'] = 'Bearer ' + auth.getToken()
    return request;
})
    
axios.interceptors.response.use(ok => ok, error => {
    if(axios.isAxiosError(error)){
        if(error.response?.status === 401){
            auth.login()
        }
    }
    throw error;
})

export const searchAlbums = new SearchAlbumsService(
    'https://api.spotify.com/v1/search',
    auth
)


