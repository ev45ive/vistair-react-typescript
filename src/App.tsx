import React from 'react';
// import logo from './logo.svg';
import 'bootstrap/dist/css/bootstrap.css';
import { PlaylistsView } from './playlists/containers/PlaylistsView';
import { AlbumSearchView } from './search/containers/AlbumSearchView';
import { Redirect, Route, Switch } from 'react-router';
import { Navbar } from './core/components/Navbar';
import { PlaylistsReduxView } from './playlists/containers/PlaylistsRedux';

function App() {
  return (
    <div className="App">
      <Navbar/>

      {/* .container>.row>.col */}
      <div className="container">
        <div className="row">
          <div className="col">

            <Switch>
              <Redirect path="/" exact={true} to="/playlists" />

              <Route path="/playlists" component={PlaylistsReduxView} />
              <Route path="/search" component={AlbumSearchView} />

              <Route path="*" render={()=><p>Page not found!</p>} />
            </Switch>

          </div>
        </div>
      </div>

    </div>
  );
}

export default App;
