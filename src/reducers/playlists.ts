import { createAction, createSlice, PayloadAction } from '@reduxjs/toolkit'
import { Playlist } from '../core/model/Playlist'




interface PlaylistsState {
    selectedId: string | null;
    items: Playlist[],
}

// const updatePlaylist = createAction('create', (payload: Playlist) => ({ payload }))
// updatePlaylist.type === 'create'
// typeof updatePlaylist ==== {type:'create', payload:Playlist}

export const counterSlice = createSlice({
    name: 'playlists',
    initialState: {
        selectedId: null,
        items: []
    } as PlaylistsState,
    reducers: {
        load: (state, action: PayloadAction<Playlist[]>) => {
            state.items = action.payload
        },
        select: (state, action: PayloadAction<Playlist['id']>) => {
            state.selectedId = action.payload
        },
        update: (state, action: PayloadAction<Playlist>) => {
            state.items = state.items.map(p => p.id === action.payload.id ? action.payload : p)
        },
        add: (state, action: PayloadAction<Playlist>) => {
            state.items.push(action.payload)
        },
        remove: (state, action: PayloadAction<Playlist['id']>) => {
            state.items = state.items.filter(p => p.id !== state.selectedId)
        },
    },
})

// Action creators are generated for each case reducer function
export const { add, load, remove, update, select } = counterSlice.actions

export default counterSlice.reducer