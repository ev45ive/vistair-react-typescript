import { Track } from "./Search";

export interface Playlist {
    id: string;
    name: string;
    public: boolean;
    description: string;
    tracks?: Track[]
}

const playlist: Playlist = {} as Playlist;

if (playlist.tracks) {
    playlist.tracks.length
}
const count1 = playlist.tracks ? playlist.tracks.length : 0
const count2 = playlist.tracks && playlist.tracks.length
const count3 = playlist.tracks?.length
const count4 = playlist.tracks?.length || 0
const count5 = (playlist.tracks ?? []).length

// const x: Array<string> | Array<number> = []
// x.length


interface Vector /* extends Point */ { x: number, y: number, length: number }
interface Point { x: number, y: number }

let v: Vector = { x: 123, y: 324, length: 234 };
let p: Point = { x: 123, y: 324 };

// p = v; // OK!
// v = p; // Error

const x: string | number = '123' as string | number
// type maybestring = typeof x

if ('string' === typeof x) {
    x.toUpperCase()
} else {
    x.toExponential()
}

const y: string | undefined = undefined;