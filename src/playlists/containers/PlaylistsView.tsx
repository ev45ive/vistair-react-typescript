// tsrafc
import React, { useEffect, useState } from 'react'
import { Playlist } from '../../core/model/Playlist'
import { PlaylistDetails } from '../components/PlaylistDetails'
import { PlaylistForm } from '../components/PlaylistForm'
import { PlaylistList } from '../components/PlaylistList'

interface Props {

}


const playlistsMockData: Playlist[] = [{
    id: '123',
    name: 'Playlist 123',
    public: true,
    description: 'I like 123'
}, {
    id: '234',
    name: 'Playlist 234',
    public: true,
    description: 'I like 234'
}, {
    id: '345',
    name: 'Playlist 345',
    public: true,
    description: 'I like 345'
}]

const playlistMockData: Playlist = playlistsMockData[1]

export const PlaylistsView = (props: Props) => {
    const [selectedId, setSelectedId] = useState<Playlist['id'] | undefined>()
    const [mode, setMode] = useState<'details' | 'edit' | 'create'>('details')
    const [playlists, setPlaylists] = useState<Playlist[]>(playlistsMockData)
    const [selectedPlaylist, setSelectedPlaylist] = useState<Playlist | undefined>()

    useEffect(() => {
        setSelectedPlaylist(playlists.find(p => p.id === selectedId))
    }, [selectedId, playlists])

    const editMode = () => { setMode('edit') }
    const detailsMode = () => { setMode('details') }
    const savePlaylist = (draft: Playlist) => {
        setPlaylists(playlists => playlists.map(p => p.id === draft.id ? draft : p))
        setMode('details')
    }
    const addPlaylist = (draft: Playlist) => {
        setPlaylists(playlists => [...playlists, draft])
        setSelectedId(draft.id)
        setMode('details')
    }
    const removePlaylist = (id: Playlist['id']) => {
        setPlaylists(playlists => playlists.filter(p => p.id !== id))
        // setSelectedId(undefined)
        setMode('details')
    }

    const createMode = () => {
        setMode('create')
        setSelectedPlaylist({
            id: Date.now().toString(), name: '', public: false, description: ''
        })
    }

    return (
        <div>
            PlaylistsView

            {/* .row>.col*2 */}
            <div className="row">
                <div className="col">
                    <PlaylistList
                        playlists={playlists}
                        selectedId={selectedId}
                        onSelect={setSelectedId}
                        onRemove={removePlaylist}
                    />

                    <button className="btn w-100 btn-info mt-3" onClick={createMode}>
                        Create New Playlist
                    </button>
                </div>
                <div className="col">

                    {mode === 'details' && selectedPlaylist && <PlaylistDetails
                        playlist={selectedPlaylist}
                        onEdit={editMode} />}

                    {mode === 'edit' && selectedPlaylist && <PlaylistForm
                        playlist={selectedPlaylist}
                        onCancel={detailsMode}
                        onSave={savePlaylist} />}

                    {mode === 'create' && selectedPlaylist && <PlaylistForm
                        playlist={selectedPlaylist}
                        onCancel={detailsMode}
                        onSave={addPlaylist} />}

                    {!selectedPlaylist && <p>Please select playlist</p>}
                </div>
            </div>
        </div>
    )
}
