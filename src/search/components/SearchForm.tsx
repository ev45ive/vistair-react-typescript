import React, { useRef, useState } from 'react'

interface Props {
    onSearch(query: string): void
}

export const SearchForm = ({ onSearch }: Props) => {
    const [query, setQuery] = useState('')
    const queryRef = useRef<HTMLInputElement>(null)

    return (
        <div>

            <div className="input-group mb-3">
                <input type="text" className="form-control" placeholder="Search"
                    ref={queryRef} value={query} onChange={e => setQuery(e.currentTarget.value)} />
                <button className="btn btn-outline-secondary" type="button" onClick={() => onSearch(query)}>Search</button>
            </div>

        </div>
    )
}
