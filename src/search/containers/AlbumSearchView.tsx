import React, { Reducer, useEffect, useReducer, useState } from 'react'
import { AlbumView } from '../../core/model/Search'
import { AlbumGrid } from '../components/AlbumGrid'
import { SearchForm } from '../components/SearchForm'
import { searchAlbums } from '../../core/services'
import { Loading } from '../../core/components/Loading'
import { useFetch } from '../../core/hooks/useFetch'

interface Props { }

interface State {
    query: string, results: AlbumView[], message: string, isLoading: boolean
}

const initialState: State = { query: '', results: [], isLoading: false, message: '' }

type SEARCH_START = { type: 'SEARCH_START', payload: { query: string } }
type SEARCH_SUCCESS = { type: 'SEARCH_SUCCESS', payload: { results: AlbumView[] } }
type SEARCH_FAILED = { type: 'SEARCH_FAILED', payload: { error: Error } }
type Actions =
    | SEARCH_START
    | SEARCH_SUCCESS
    | SEARCH_FAILED
const searchStart = (query: string): SEARCH_START => ({ type: 'SEARCH_START', payload: { query } })
const searchSuccess = (results: AlbumView[]): SEARCH_SUCCESS => ({ type: 'SEARCH_SUCCESS', payload: { results } })
const searchFailed = (error: Error): SEARCH_FAILED => ({ type: 'SEARCH_FAILED', payload: { error } })

const reducer: Reducer<State, Actions> = (state, action) => {
    switch (action.type) {
        case 'SEARCH_START': { return { ...state, query: action.payload.query, message: '', isLoading: true } }
        case 'SEARCH_SUCCESS': { return { ...state, results: action.payload.results, isLoading: false } }
        case 'SEARCH_FAILED': { return { ...state, message: action.payload.error?.message, isLoading: false } }
        default: return state
    }
}

export const AlbumSearchView = (props: Props) => {
    // const [{ results, message, isLoading }, sendRequest] = useFetch(
    //     (query: string) => searchAlbums.search(query)
    // )

    const [{
        isLoading, message, query, results
    }, dispatch] = useReducer(reducer, initialState)

    const sendRequest = async (query: string) => {
        try {
            dispatch(searchStart(query))
            const results = await searchAlbums.search(query)
            dispatch(searchSuccess(results))
        } catch (error) {
            dispatch(searchFailed(error))
        }
    }

    return (
        <div>
            AlbumSearchView

            <div className="row">
                <div className="col">
                    <SearchForm onSearch={sendRequest} />
                </div>
            </div>
            <div className="row">
                <div className="col">
                    {message && <p className="alert alert-danger">{message}</p>}
                    {isLoading && <Loading />}
                    {results && <AlbumGrid albums={results} />}
                </div>
            </div>
        </div>
    )
}
