// tsrafc
import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector, useStore } from 'react-redux'
import { Playlist } from '../../core/model/Playlist'
import { add, load, remove, select, update } from '../../reducers/playlists'
import { AppState } from '../../store'
import { PlaylistDetails } from '../components/PlaylistDetails'
import { PlaylistForm } from '../components/PlaylistForm'
import { PlaylistList } from '../components/PlaylistList'

interface Props {

}

const playlistsMockData: Playlist[] = [{
    id: '123',
    name: 'Playlist 123',
    public: true,
    description: 'I like 123'
}, {
    id: '234',
    name: 'Playlist 234',
    public: true,
    description: 'I like 234'
}, {
    id: '345',
    name: 'Playlist 345',
    public: true,
    description: 'I like 345'
}]


export const PlaylistsReduxView = (props: Props) => {
    const [mode, setMode] = useState<'details' | 'edit' | 'create'>('details')

    // useStore()
    const dispatch = useDispatch()
    const playlists = useSelector((state:AppState) => state.playlists.items)
    const selectedPlaylist = useSelector((state:AppState) => state.playlists.items.find(p => p.id === state.playlists.selectedId))
    
    useEffect(()=>{
        dispatch(load(playlistsMockData))
    },[])

    const editMode = () => { setMode('edit') }
    const detailsMode = () => { setMode('details') }
    const savePlaylist = (draft: Playlist) => {
        setMode('details')
        dispatch(update(draft))
    }
    const addPlaylist = (draft: Playlist) => {
        setMode('details')
        dispatch(add(draft))
    }
    const removePlaylist = (id: Playlist['id']) => {
        setMode('details')
        dispatch(remove(id))
    }
    const createMode = () => {
        setMode('create')
    }

    return (
        <div>
            PlaylistsView

            {/* .row>.col*2 */}
            <div className="row">
                <div className="col">
                    <PlaylistList
                        playlists={playlists}
                        selectedId={selectedPlaylist?.id}
                        onSelect={(id) => {
                            dispatch(select(id))
                        }}
                        onRemove={removePlaylist}
                    />

                    <button className="btn w-100 btn-info mt-3" onClick={createMode}>
                        Create New Playlist
                    </button>
                </div>
                <div className="col">

                    {mode === 'details' && selectedPlaylist && <PlaylistDetails
                        playlist={selectedPlaylist}
                        onEdit={editMode} />}

                    {mode === 'edit' && selectedPlaylist && <PlaylistForm
                        playlist={selectedPlaylist}
                        onCancel={detailsMode}
                        onSave={savePlaylist} />}

                    {mode === 'create' && selectedPlaylist && <PlaylistForm
                        playlist={selectedPlaylist}
                        onCancel={detailsMode}
                        onSave={addPlaylist} />}

                    {!selectedPlaylist && <p>Please select playlist</p>}
                </div>
            </div>
        </div>
    )
}
