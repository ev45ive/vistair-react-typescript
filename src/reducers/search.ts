import { AnyAction, Reducer } from "redux"
import { AlbumView } from "../core/model/Search"

interface State {
    query: string, results: AlbumView[], message: string, isLoading: boolean
}

const initialState: State = { query: '', results: [], isLoading: false, message: '' }

type SEARCH_START = { type: 'SEARCH_START', payload: { query: string } }
type SEARCH_SUCCESS = { type: 'SEARCH_SUCCESS', payload: { results: AlbumView[] } }
type SEARCH_FAILED = { type: 'SEARCH_FAILED', payload: { error: Error } }
type Actions =
    | SEARCH_START
    | SEARCH_SUCCESS
    | SEARCH_FAILED
const searchStart = (query: string): SEARCH_START => ({ type: 'SEARCH_START', payload: { query } })
const searchSuccess = (results: AlbumView[]): SEARCH_SUCCESS => ({ type: 'SEARCH_SUCCESS', payload: { results } })
const searchFailed = (error: Error): SEARCH_FAILED => ({ type: 'SEARCH_FAILED', payload: { error } })

const reducer: Reducer<State, AnyAction> = (state = initialState, action) => {
    switch (action.type) {
        case 'SEARCH_START': { return { ...state, query: action.payload.query, message: '', isLoading: true } }
        case 'SEARCH_SUCCESS': { return { ...state, results: action.payload.results, isLoading: false } }
        case 'SEARCH_FAILED': { return { ...state, message: action.payload.error?.message, isLoading: false } }
        default: return state
    }
}

export default  reducer