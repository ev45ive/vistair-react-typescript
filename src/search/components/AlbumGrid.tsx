import React from 'react'
import { AlbumView } from '../../core/model/Search'
import { AlbumCard } from './AlbumCard'

interface Props {
    albums:AlbumView[]
}

export const AlbumGrid = ({albums}: Props) => {
    return (
        <div className="row row-cols-1 row-cols-sm-4 g-0">
            {albums.map(album => <div className="col" key={album.id}>
                <AlbumCard album={album}/>
            </div>)}
        </div>
    )
}
