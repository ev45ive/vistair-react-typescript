import axios, { AxiosError } from "axios";
import { AlbumsSearchResponse, AlbumView, PagingObject } from "../model/Search";

interface AbstractSearchService<T> {
    search(query: string): Promise<T[]>
}

interface AuthProvider {
    getToken(): string
}

export class SearchAlbumsService implements AbstractSearchService<AlbumView>{
    // private api_url: string;

    constructor(private api_url: string, private auth: AuthProvider) {
        // this.api_url = api_url
    }


    search(query: string) {
        const resp = axios.get<AlbumsSearchResponse>(this.api_url, {
           
            params: {
                type: 'album', q: query
            }
        })
        return resp
            .then(res => res.data.albums.items)
            .catch(error => {
                if (!axios.isAxiosError(error)) { throw new Error('Unexpected error!') }

                if (isSpotifyError(error.response?.data)) {
                    throw new Error(error.response?.data.error.message)
                } else {
                    throw new Error(error?.message || 'Unexpected error!')
                }
                // return Promise.reject(new Error(error.response.data.error.message))
                // return []
            })
    }
}


// function isAxiosError<T>(error: any): error is AxiosError<T> {
//     return !!error.AxiosError
// }
interface SpotifyError { error: { message: string } }

function isSpotifyError(error: any): error is SpotifyError {
    debugger
    return error?.error?.message
}