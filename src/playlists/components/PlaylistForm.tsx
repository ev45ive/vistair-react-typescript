import React, { useEffect, useRef, useState } from 'react'
import { Playlist } from '../../core/model/Playlist'

interface Props {
    playlist: Playlist
    onCancel(): void
    onSave(draft: Playlist): void
}

// keeps state here!
// rerun the PlaylistForm function with new statea 
export const PlaylistForm = ({ playlist, onCancel, onSave }: Props) => {
    const [name, setName] = useState(playlist.name)
    const [isPublic, setIsPublic] = useState(playlist.public)
    const [description, setDescription] = useState(playlist.description)
    const nameRef = useRef<HTMLInputElement>(null)

    useEffect(() => {
        setName(playlist.name)
        setIsPublic(playlist.public)
        setDescription(playlist.description)
    }, [playlist])

    const handleChange = ({ currentTarget: { value } }: React.ChangeEvent<HTMLInputElement>) => {
        setName(value)
    }
    const save = () => {
        onSave({ id: playlist.id, name, public: isPublic, description })
    }

    useEffect(() => {
        nameRef.current?.focus()
    }, [])

    return (
        <div>
            PlaylistForm
            <pre>{JSON.stringify(playlist, null, 2)}</pre>
            <pre>{JSON.stringify({ name, public: isPublic, description }, null, 2)}</pre>

            {/* .form-group>label{Name:}+input.form-control */}
            <div className="form-group mb-2">
                <label>Name:</label>
                <input type="text" className="form-control" value={name} onChange={handleChange} ref={nameRef} />
                <p>{name.length} / 170</p>
            </div>

            <div className="form-check mb-2">
                <input className="form-check-input" type="checkbox" checked={isPublic}
                    onChange={e => setIsPublic(e.currentTarget.checked)} />
                <label className="form-check-label" htmlFor="flexCheckDefault">
                    Public
                </label>
            </div>

            <div className="form-group mb-2">
                <label>Description:</label>
                <textarea className="form-control" value={description} onChange={e => setDescription(e.currentTarget.value)}></textarea>
            </div>

            <button className="btn btn-danger" onClick={onCancel}>Cancel</button>
            <button className="btn btn-success" onClick={save}>Save</button>
        </div>
    )
}
