import React from 'react'

interface Props {

}

export const Loading = (props: Props) => {
    return (
        <div>
            <div className="d-flex justify-content-center">
                <div className="spinner-border" role="status">
                    <span className="visually-hidden">Loading...</span>
                </div>
            </div>
        </div>
    )
}
