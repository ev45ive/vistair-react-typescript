import { configureStore } from '@reduxjs/toolkit'
import counter from './reducers/counter'
import playlists from './reducers/playlists'
import search from './reducers/search'

const store = configureStore({
    reducer: {
        counter: counter, 
        search: search,
        playlists: playlists
    },
})

export type AppState = ReturnType<(typeof store['getState'])>


export default store