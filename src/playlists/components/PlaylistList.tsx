import React, { useState } from 'react'
import { Playlist } from '../../core/model/Playlist'

interface Props {
    playlists: Playlist[]
    onSelect(playlist_id: Playlist['id']): void
    selectedId?: Playlist['id']
    onRemove(id: Playlist['id']): void
}

export const PlaylistList = ({ playlists, onSelect, onRemove, selectedId }: Props) => {

    return (
        <div className="tabs">
            <div className="list-group list-group-flush" role="tablist" >
                {playlists.map((p, index) => <button role="tab" aria-selected={p.id === selectedId}
                    className={"list-group-item list-group-item-action " + (p.id === selectedId ? 'active' : '')}
                    onClick={() => {
                        onSelect(p.id)
                    }} key={p.id}>

                    <span>
                        {index + 1}. {p.name}
                    </span>

                    <span className="close float-end" onClick={() => onRemove(p.id)}>&times;</span>
                </button>)}
            </div>
        </div>
    )
}

